#=================
#
# RPI_SDCARD_WRITE
#
# Write image to sd card.
#
#=================
RPI_SDCARD_WRITE()
{
    SPACE_SIGNATURE="image device"
    SPACE_DEP="PRINT OS_COMMAND"

    local image="${1}"
    shift

    local device="${1}"
    shift

    if [ "${device}" = "/dev/sda" ]; then
        PRINT "Cowardly refusing to write to /dev/sda." "error"
        return 1
    fi


    if [ "${image}" = "-" ]; then
        image=
    elif [ ! -b  "${device}" ]; then
        PRINT "Device ${device} does not exist" "error"
        return 1
    fi

    PRINT "Write image ${image} to ${device}."
    if OS_COMMAND "pv" >/dev/null; then
        pv "${image}" -e -p -r | dd of="${device}" bs=1MB
    else
        dd if="${image}" of="${device}" bs=1MB
    fi
    if [ "$?" -gt 0 ]; then
        if [ $(id -u) -gt 0 ]; then
            PRINT "You will need root permissions to write to the sdcard, use -s sudo" "error"
        fi
        return 1
    fi
    hdparm -z "${device}"
}

#=========================
#
# RPI_SDCARD_SDCARD_STATUS
#
#
#=========================
RPI_SDCARD_SDCARD_STATUS()
{
    SPACE_SIGNATURE="device"

    local device="${1}"
    shift

    df ${device}
}
#========================
#
# RPI_SDCARD_SDCARD_MOUNT
#
# Mount boot and root partitions.
#
#========================
RPI_SDCARD_SDCARD_MOUNT()
{
    SPACE_SIGNATURE="device bootpartition rootpartition mnt flags:0"
    SPACE_DEP="PRINT OS_COMMAND"

    local device="${1}"
    shift

    local bootpartition="${1}"
    shift

    local rootpartition="${1}"
    shift

    local mnt="${1}"
    shift

    local flags="${1}"
    shift

    local mntboot="${mnt}/boot"
    local mntroot="${mnt}/root"

    # TODO: mountpoint is linux specific.
    if OS_COMMAND "mountpoint" >/dev/null; then
        if mountpoint "${mnt}" >/dev/null 2>&1; then
            PRINT "${mnt} is a mountpoint" "error"
            return 1
        fi
        if mountpoint "${mntboot}" >/dev/null 2>&1; then
            PRINT "${mntboot} is a mountpoint" "error"
            return 1
        fi
        if mountpoint "${mntroot}" >/dev/null 2>&1; then
            PRINT "${mntroot} is a mountpoint" "error"
            return 1
        fi
    fi

    mkdir -p "${mntboot}"
    mkdir -p "${mntroot}"

    local pboot="${device}${bootpartition}"
    local proot="${device}${rootpartition}"

    if mount ${flags} "${pboot}" "${mntboot}"; then
        PRINT "Boot partition mounted at: ${mntboot}."
    fi &&
    if mount ${flags} "${proot}" "${mntroot}"; then
        PRINT "Root partition mounted at: ${mntroot}."
    fi
}

#=========================
#
# RPI_SDCARD_SDCARD_UMOUNT
#
# Unmount boot and root partitions.
#
#=========================
RPI_SDCARD_SDCARD_UMOUNT()
{
    SPACE_SIGNATURE="device bootpartition rootpartition"

    local device="${1}"
    shift

    local bootpartition="${1}"
    shift

    local rootpartition="${1}"
    shift

    local pboot="${device}${bootpartition}"
    local proot="${device}${rootpartition}"

    umount "${pboot}"
    umount "${proot}"
}

#=======================
#
# RPI_SDCARD_STATUS_BOOT
#
# Show status of mounted boot partition.
#
#=======================
RPI_SDCARD_STATUS_BOOT()
{
    SPACE_SIGNATURE="mountpoint"
    SPACE_DEP="PRINT"

    local mntpoint="${1}"
    shift

    if ! mountpoint "${mntpoint}" >/dev/null; then
        PRINT "${mntpoint} is not a mountpoint" "error"
        return 1
    fi

    PRINT "${mntpoint} is a mountpoint" "ok"
    if [ -f "${mntpoint}/os-release" ]; then
        PRINT "/os-release detected" "ok"
    else
        PRINT "/os-release not detected" "warning"
    fi
    if [ -f "${mntpoint}/device-init.yaml" ]; then
        PRINT "/device-init.yaml detected" "ok"
    else
        PRINT "/device-init.yaml not detected" "warning"
    fi
    if [ -f "${mntpoint}/config.txt" ]; then
        PRINT "/config.txt detected" "ok"
    else
        PRINT "/config.txt not detected" "warning"
    fi
}

#=======================
#
# RPI_SDCARD_STATUS_ROOT
#
# Show status of mounted root partition.
#
#=======================
RPI_SDCARD_STATUS_ROOT()
{
    SPACE_SIGNATURE="mountpoint"
    SPACE_DEP="PRINT"

    local mntpoint="${1}"
    shift

    if ! mountpoint "${mntpoint}" >/dev/null; then
        PRINT "${mntpoint} is not a mountpoint" "error"
        return 1
    fi

    PRINT "${mntpoint} is a mountpoint" "ok"
    if [ -f "${mntpoint}/etc/os-release" ]; then
        PRINT "/etc/os-release detected" "ok"
    else
        PRINT "/etc/os-release not detected" "warning"
    fi
    if [ -f "${mntpoint}/bin/spaceagent.sh" ]; then
        PRINT "/bin/spaceagent.sh detected" "ok"
    else
        PRINT "/bin/spaceagent.sh not detected"
    fi
    PRINT "Use space -m systemd -e root=${mntpoint} to manage services."
}

#====================
#
# RPI_SDCARD_CP_FILE
#
# Copy a file.
#
#====================
RPI_SDCARD_CP_FILE()
{
    SPACE_SIGNATURE="root file dest"

    local root="${1}"
    shift

    local file="${1}"
    shift

    local dest="${1}"
    shift

    case "${root}" in
        */)
            root="${root%/}"
            ;;
    esac

    cp "${file}" "${root}${dest}"
}
